
## DB Diagram
https://dbdiagram.io/d/6231a0f20ac038740c42cd18

## Swagger
HOST:PORT/api

## Installation

```bash
$ npm install
```

## Running the app

```bash
# docker
$ docker compose up -d

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Seeding
```bash
$ npm run seed
```
