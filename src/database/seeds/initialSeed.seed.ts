import { Factory, Seeder } from 'typeorm-seeding';
import { Event } from '../../events/entities/event.entity';
import { Screen } from '../../screens/entities/screen.entity';
import { Playlist } from '../../playlists/entities/playlist.entity';
import { Content } from '../../content/entities/content.entity';
import { PlaylistContent } from '../../playlist-content/entities/playlist-content.entity';

export class InitialSeed implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const events: Event[] = await factory(Event)().createMany(15);

    const screens: Screen[] = await Promise.all(
      events.map(async (event) => {
        return await factory(Screen)().create({
          user: event.user,
          event: event,
        });
      }),
    );

    const playlists: Playlist[] = await Promise.all(
      screens.map(async (screen) => {
        return await factory(Playlist)().create({
          user: screen.user,
          screen: screen,
        });
      }),
    );

    playlists.map(async (playlist) => {
      const content: Content = await factory(Content)().create({
        user: playlist.user,
      });

      await factory(PlaylistContent)().create({
        playlist: playlist,
        content: content,
      });
    });
  }
}
