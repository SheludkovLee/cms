import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { PlaylistContent } from '../../playlist-content/entities/playlist-content.entity';
import { User } from '../../users/entities/user.entity';
import { IsString, Length } from 'class-validator';
import { Screen } from '../../screens/entities/screen.entity';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '../../base/entities/base-entity';

@Entity('playlists')
export class Playlist extends BaseEntity {
  @ApiProperty({
    example: 'playlist name',
    minLength: 2,
    maxLength: 20,
  })
  @Column('varchar')
  @IsString()
  @Length(2, 20)
  name!: string;

  @Column('int')
  userId!: User['id'];

  @ManyToOne(() => User, (user) => user.playlists, {
    onDelete: 'CASCADE',
  })
  user!: User;

  @ApiProperty({ type: Number })
  @Column('int')
  screenId!: Screen['id'];

  @OneToOne(() => Screen, (screen) => screen.playlist, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  screen!: Screen;

  @OneToMany(
    () => PlaylistContent,
    (playlistContent) => playlistContent.playlist,
  )
  playlistContent!: PlaylistContent[];
}
