import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Playlist])],
  providers: [PlaylistsService],
  exports: [PlaylistsService],
  controllers: [PlaylistsController],
})
export class PlaylistsModule {}
