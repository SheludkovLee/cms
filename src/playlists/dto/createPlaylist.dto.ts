import { PickType } from '@nestjs/swagger';
import { Playlist } from '../entities/playlist.entity';

export class CreatePlaylistDto extends PickType(Playlist, ['name']) {}
