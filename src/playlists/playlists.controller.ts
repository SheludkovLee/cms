import { Controller, UseGuards } from '@nestjs/common';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { Playlist } from './entities/playlist.entity';
import { PlaylistsService } from './playlists.service';
import { CreatePlaylistDto } from './dto/createPlaylist.dto';
import { UpdatePlaylistDto } from './dto/updatePlaylist.dto';
import { User } from '../users/entities/user.entity';
import { IsOwnerPlaylistGuard } from './guards/is-owner-playlist.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsOwnerParentPlaylistGuard } from './guards/is-owner-parent-playlist.guard';

@ApiTags('playlists')
@ApiBearerAuth('JWT-auth')
@Crud({
  model: {
    type: Playlist,
  },
  dto: {
    create: CreatePlaylistDto,
    update: UpdatePlaylistDto,
    replace: CreatePlaylistDto,
  },
  params: {
    screenId: {
      field: 'screenId',
      type: 'number',
    },
  },
  routes: {
    exclude: ['createManyBase'],
    createManyBase: {
      decorators: [UseGuards(IsOwnerParentPlaylistGuard)],
    },
    createOneBase: {
      decorators: [UseGuards(IsOwnerParentPlaylistGuard)],
    },
    updateOneBase: {
      decorators: [UseGuards(IsOwnerPlaylistGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(IsOwnerPlaylistGuard)], //TODO
    },
    deleteOneBase: {
      decorators: [UseGuards(IsOwnerPlaylistGuard)],
    },
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id,
  }),
})
@Controller('screens/:screenId/playlist')
export class PlaylistsController implements CrudController<Playlist> {
  constructor(public service: PlaylistsService) {}
}
