import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Playlist } from '../entities/playlist.entity';

export const IsOwnerPlaylistGuard = new IsOwnerGuard(Playlist);
