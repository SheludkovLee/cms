import { ApiProperty, PickType } from '@nestjs/swagger';
import { Content } from '../entities/content.entity';
import { CreateFileDto } from '../../files/dto/create-file.dto';
import { IsArray, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateContentDto extends PickType(Content, ['contentType']) {
  @ApiProperty({ type: CreateFileDto, isArray: true })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateFileDto)
  files!: CreateFileDto[];
}
