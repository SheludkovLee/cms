import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Content } from '../entities/content.entity';

export const IsOwnerContentGuard = new IsOwnerGuard(Content);
