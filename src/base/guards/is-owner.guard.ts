import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { EntityTarget, getRepository, Repository } from 'typeorm';
import { User } from '../../users/entities/user.entity';

@Injectable()
export class IsOwnerGuard<Entity extends { userId: User['id'] }>
  implements CanActivate
{
  constructor(
    private entity: EntityTarget<Entity>,
    private idParam: string = 'id',
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    console.log('1');
    const req: any = context.switchToHttp().getRequest();
    const { user, params, body } = req;

    if (!user || (!params && !body)) {
      return false;
    }

    const authUserId: number = user.id;
    const idItem = this.getItemId(req);

    const repo: Repository<Entity> = getRepository(this.entity);

    const item: Entity | undefined = await repo.findOne(idItem);

    if (!item) {
      return true;
    }

    return item.userId === authUserId;
  }

  protected getItemId(req: any): number {
    const { params } = req;
    return Number(params[this.idParam]);
  }
}
