import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Event } from '../../events/entities/event.entity';

export const IsOwnerParentScreenGuard = new IsOwnerGuard(Event, 'eventId');
