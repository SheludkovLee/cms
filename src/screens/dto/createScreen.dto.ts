import { PickType } from '@nestjs/swagger';
import { Screen } from '../entities/screen.entity';

export class CreateScreenDto extends PickType(Screen, ['name']) {}
