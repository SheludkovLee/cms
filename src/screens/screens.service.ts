import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Screen } from './entities/screen.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ScreensService extends TypeOrmCrudService<Screen> {
  constructor(@InjectRepository(Screen) repo: Repository<Screen>) {
    super(repo);
  }
}
