import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Screen } from './entities/screen.entity';
import { ScreensService } from './screens.service';
import { ScreensController } from './screens.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Screen])],
  providers: [ScreensService],
  exports: [ScreensService],
  controllers: [ScreensController],
})
export class ScreensModule {}
