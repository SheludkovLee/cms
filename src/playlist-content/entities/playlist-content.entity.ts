import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Playlist } from '../../playlists/entities/playlist.entity';
import { Content } from '../../content/entities/content.entity';
import { IsInt, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Entity('playlist_content')
export class PlaylistContent {
  @ApiProperty({
    description: 'Content duration',
  })
  @Column('float')
  @IsPositive()
  duration!: number;

  @ApiProperty({
    description: 'Content position in playlist',
  })
  @Column('int')
  @IsPositive()
  @IsInt()
  position!: number;

  @ApiProperty({ type: Number })
  @PrimaryColumn('int')
  playlistId!: Playlist['id'];

  @ApiProperty({ type: Number })
  @PrimaryColumn('int')
  contentId!: Content['id'];

  @ManyToOne(() => Playlist, (playlist) => playlist.playlistContent, {
    onDelete: 'CASCADE',
  })
  playlist!: Playlist;

  @ManyToOne(() => Content, (content) => content.playlistContent, {
    onDelete: 'CASCADE',
  })
  content!: Content;
}
