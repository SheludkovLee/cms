import { PartialType } from '@nestjs/swagger';
import { CreatePlaylistContentDto } from './createPlaylistContent.dto';

export class UpdatePlaylistContentDto extends PartialType(
  CreatePlaylistContentDto,
) {}
