import { PickType } from '@nestjs/swagger';
import { PlaylistContent } from '../entities/playlist-content.entity';

export class CreatePlaylistContentDto extends PickType(PlaylistContent, [
  'duration',
  'position',
]) {}
