import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../../users/services/users.service';
import { HashService } from '../../users/services/hash.service';
import { User } from '../../users/entities/user.entity';
import { ResponseLoginDto } from '../dto/response-login.dto';
import { ResponseRegisterDto } from '../dto/response-registration.dto';
import { plainToClass } from 'class-transformer';
import { CreateUserDto } from '../../users/dto/createUser.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private hashService: HashService,
  ) {}

  async validateUser(
    email: User['email'],
    password: User['password'],
  ): Promise<User | null> {
    const user = await this.usersService.findOne({ where: { email: email } });
    if (!user) {
      return null;
    }
    const isRealPassword = await this.hashService.compareHash(
      password,
      user.password,
    );
    if (isRealPassword) {
      return user;
    }
    return null;
  }

  async login(user: User): Promise<ResponseLoginDto> {
    const payload = { email: user.email, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async registration(dto: CreateUserDto): Promise<ResponseRegisterDto> {
    const candidate = await this.usersService.findOne({
      where: { email: dto.email },
    });
    if (candidate) {
      throw new BadRequestException(
        `User with email: ${dto.email} already exists`,
      );
    }
    const newUser = await this.usersService.registerOne(dto);

    const { access_token } = await this.login(newUser);
    return {
      access_token,
      user: plainToClass(User, newUser),
    };
  }
}
