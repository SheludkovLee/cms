import { User } from '../../users/entities/user.entity';
import { ResponseLoginDto } from './response-login.dto';

export class ResponseRegisterDto extends ResponseLoginDto {
  user!: User;
}
