import { Module } from '@nestjs/common';
import { FilesCloudService } from './servises/files-cloud.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Content } from '../content/entities/content.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Content])],
  providers: [FilesCloudService],
  exports: [FilesCloudService],
})
export class FilesModule {}
