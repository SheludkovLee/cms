import { Controller, UseGuards } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';

import { UsersService } from './services/users.service';
import { User } from './entities/user.entity';
import { IsOwnerAccountGuard } from './guards/is-owner-account.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UpdateUserDto } from './dto/updateUser.dto';

@ApiTags('users')
@ApiBearerAuth('JWT-auth')
@Crud({
  model: {
    type: User,
  },
  dto: {
    update: UpdateUserDto,
  },
  routes: {
    exclude: ['createOneBase', 'createManyBase', 'replaceOneBase'],
    deleteOneBase: {
      decorators: [UseGuards(IsOwnerAccountGuard)],
    },
    updateOneBase: {
      decorators: [UseGuards(IsOwnerAccountGuard)],
    },
  },
})
@Controller('users')
export class UsersController implements CrudController<User> {
  constructor(public service: UsersService) {}
}
