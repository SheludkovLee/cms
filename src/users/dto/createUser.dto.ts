import { IntersectionType, PickType } from '@nestjs/swagger';
import { User } from '../entities/user.entity';
import { UserPasswordDto } from './user-password.dto';

export class CreateUserDto extends IntersectionType(
  PickType(User, ['email']),
  UserPasswordDto,
) {}
