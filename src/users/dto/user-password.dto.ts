import { IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserPasswordDto {
  @ApiProperty({
    example: 'password',
    minLength: 6,
    maxLength: 50,
  })
  @IsString()
  @Length(6, 50)
  password!: string;
}
